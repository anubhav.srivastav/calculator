package com.sezzle.calculator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sezzle.calculator.exceptions.DivideByZeroException;
import com.sezzle.calculator.exceptions.WrongExpressionException;
import com.sezzle.calculator.services.ClaculatorService;
import com.sezzle.calculator.services.ICalculatorService;

class Calculator implements ActionListener {

	private ICalculatorService claculatorServiceImpl = new ClaculatorService();
	
	@Override	
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if (command.charAt(0) == 'C') {
			Main.inputBox.setText("");
		} else if (command.charAt(0) == '=') {
			try {
				Main.inputBox.setText(claculatorServiceImpl.calculate(Main.inputBox.getText()));
			} catch (DivideByZeroException e1) {
				System.out.println(e1.getMessage());
			} catch (WrongExpressionException e1) {
				System.out.println(e1.getMessage());
			}
		} else {
			Main.inputBox.setText(Main.inputBox.getText() + command);
		}
	}

}
