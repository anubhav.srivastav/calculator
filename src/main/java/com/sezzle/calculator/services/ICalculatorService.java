package com.sezzle.calculator.services;

import com.sezzle.calculator.exceptions.DivideByZeroException;
import com.sezzle.calculator.exceptions.WrongExpressionException;

public interface ICalculatorService {
	String calculate(String expression) throws WrongExpressionException, DivideByZeroException ;
}
