package com.sezzle.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.reflect.Method;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.sezzle.calculator.exceptions.DivideByZeroException;
import com.sezzle.calculator.exceptions.WrongExpressionException;
import com.sezzle.calculator.services.ClaculatorService;
import com.sezzle.calculator.services.ICalculatorService;

@SpringBootTest
class CalculateServiceTests {

	private ICalculatorService calculatorService = new ClaculatorService();
	
	@Test
	void testCalculateShouldSucceed() throws Exception{
		
		String result = "";
		
		result = calculatorService.calculate("40*2+1");
		
		assertEquals(result, "81");
		
	}

	@Test
	void testCalculateShouldFailIfTheExpressionIsWrong() throws Exception{
				
		assertThrows(WrongExpressionException.class, () -> calculatorService.calculate("40*2+++1"));
				
	}

	@Test
	void testCalculateShouldFailIfWeDivideByZero() throws Exception{
				
		assertThrows(DivideByZeroException.class, () -> calculatorService.calculate("40/0"));
				
	}

}
