package com.sezzle.calculator.exceptions;

public class DivideByZeroException extends Exception{
	public DivideByZeroException(String message) {
		super(message);
	}
	
}
