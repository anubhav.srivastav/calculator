package com.sezzle.calculator.services;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.springframework.stereotype.Service;

import com.sezzle.calculator.exceptions.DivideByZeroException;
import com.sezzle.calculator.exceptions.WrongExpressionException;

public class ClaculatorService implements ICalculatorService{

	@Override
	public String calculate(String expression) throws WrongExpressionException, DivideByZeroException {
		if (!checkExpression(expression))
			throw new WrongExpressionException("The expression is wrong: you might used two commands in a row");
			
		ScriptEngineManager manager = new ScriptEngineManager();
	    ScriptEngine engine = manager.getEngineByName("JavaScript");
	    String result = "";
	    try {
			result = engine.eval(expression).toString();
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	    
	    if (result.equals("Infinity") || result.equals("-Infinity"))
	    	throw new DivideByZeroException("Divisor cannot be 0"); 

		return result;
	}

	private Boolean checkExpression(String expression) {
		char[] commands = {'+','-','*','/','.'};
		Boolean valid = true;
		for (int i = 0; i < expression.length()-1; i++) {
			char a = expression.charAt(i);
			char b = expression.charAt(i+1);
			if ( contains(a, commands) && contains(b, commands) )
				valid = false;
		}
		return valid;
	}
	
	private Boolean contains(char c, char[] array) {
	    for (char x : array) {
	        if (x == c) {
	            return true;
	        }
	    }
	    return false;
	}

}
