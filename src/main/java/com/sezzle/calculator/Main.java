package com.sezzle.calculator;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.springframework.beans.factory.annotation.Autowired;

public class Main {
	
	public static JTextField inputBox;

	@Autowired
	static Calculator c;
	
	public static void main(String[] args) {
		createWindow();
	}

	private static void createWindow() {
		JFrame frame = new JFrame("Calculator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		createUI(frame);
		frame.setSize(600, 600);
		frame.setVisible(true);
	}

	private static void createUI(JFrame frame) {
		JPanel panel = new JPanel();
		Calculator c = new Calculator();
		GridLayout layout = new GridLayout(5, 5, 5, 5);
		GridBagConstraints gbc = new GridBagConstraints();
		panel.setLayout(layout);
		inputBox = new JTextField(10);
		inputBox.setSize(100, 400);
		inputBox.setEditable(false);

		JPanel panel2 = new JPanel();
		GridLayout layout2 = new GridLayout();
		layout2.setColumns(1);
		panel2.setLayout(layout2);
		inputBox.setMargin(new Insets(20, 20, 20, 20));
		panel2.add(inputBox);

		JButton button0 = new JButton("0");
		JButton button1 = new JButton("1");
		JButton button2 = new JButton("2");
		JButton button3 = new JButton("3");
		JButton button4 = new JButton("4");
		JButton button5 = new JButton("5");
		JButton button6 = new JButton("6");
		JButton button7 = new JButton("7");
		JButton button8 = new JButton("8");
		JButton button9 = new JButton("9");

		JButton buttonPlus = new JButton("+");
		JButton buttonMinus = new JButton("-");
		JButton buttonDivide = new JButton("/");
		JButton buttonMultiply = new JButton("*");
		JButton buttonClear = new JButton("C");
		JButton buttonDot = new JButton(".");
		JButton buttonEquals = new JButton("=");

		button1.addActionListener(c);
		button2.addActionListener(c);
		button3.addActionListener(c);
		button4.addActionListener(c);
		button5.addActionListener(c);
		button6.addActionListener(c);
		button7.addActionListener(c);
		button8.addActionListener(c);
		button9.addActionListener(c);
		button0.addActionListener(c);

		buttonPlus.addActionListener(c);
		buttonMinus.addActionListener(c);
		buttonDivide.addActionListener(c);
		buttonMultiply.addActionListener(c);
		buttonClear.addActionListener(c);
		buttonDot.addActionListener(c);
		buttonEquals.addActionListener(c);

		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(button1, gbc);
		gbc.gridx = 1;
		gbc.gridy = 0;
		panel.add(button2, gbc);
		gbc.gridx = 2;
		gbc.gridy = 0;
		panel.add(button3, gbc);
		gbc.gridx = 3;
		gbc.gridy = 0;
		panel.add(buttonPlus, gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		panel.add(button4, gbc);
		gbc.gridx = 1;
		gbc.gridy = 1;
		panel.add(button5, gbc);
		gbc.gridx = 2;
		gbc.gridy = 1;
		panel.add(button6, gbc);
		gbc.gridx = 3;
		gbc.gridy = 1;
		panel.add(buttonMinus, gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		panel.add(button7, gbc);
		gbc.gridx = 1;
		gbc.gridy = 2;
		panel.add(button8, gbc);
		gbc.gridx = 2;
		gbc.gridy = 2;
		panel.add(button9, gbc);
		gbc.gridx = 3;
		gbc.gridy = 2;
		panel.add(buttonDivide, gbc);
		gbc.gridx = 0;
		gbc.gridy = 3;
		panel.add(buttonDot, gbc);
		gbc.gridx = 1;
		gbc.gridy = 3;
		panel.add(button0, gbc);
		gbc.gridx = 2;
		gbc.gridy = 3;
		panel.add(buttonClear, gbc);
		gbc.gridx = 3;
		gbc.gridy = 3;
		panel.add(buttonMultiply, gbc);
		gbc.gridwidth = 4;

		gbc.gridx = 4;
		gbc.gridy = 2;
		panel.add(buttonEquals, gbc);

		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.getContentPane().add(panel2, BorderLayout.NORTH);

	}

}
