package com.sezzle.calculator.exceptions;

public class WrongExpressionException extends Exception{
	public WrongExpressionException(String message) {
		super(message);
	}
	
}
